
#ifndef VALO_RAY_H
#define VALO_RAY_H

#include <common.h>

namespace valo {

class Ray {
public:
    glm::vec4 position;
    glm::vec4 direction;
};

} // ns valo

#endif // VALO_RAY_H
