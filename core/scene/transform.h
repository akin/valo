
#ifndef VALO_TRANSFORM_H
#define VALO_TRANSFORM_H

#include <common.h>
#include <glm/gtc/quaternion.hpp>

namespace valo {

class Transform {
public:
    std::string id;
    std::string name;
    std::string type;

    glm::vec3 position;
    glm::vec3 scale;
    glm::quat rotation;
    float32 radius = 0.0f;
private:
    glm::mat4 world;
    glm::mat4 worldinverse; // TODO, get rid of.

    // no ownership
    NoOwnershipOf(Transform) parent = nullptr;
    std::vector<NoOwnershipOf(Transform)> children;
public:
    Transform();
    ~Transform();

    void attach(Transform& child);

    void detach(Transform& child);
    void detach();

    // Calculate world matrix, for this, and children.
    void apply();
    const glm::mat4& matrix() const;
    const glm::mat4& inverseMatrix() const;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_TRANSFORM_H
