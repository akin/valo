
#include "enviroment.h"

namespace valo {

Enviroment::Enviroment()
: texture(nullptr)
{
}

Enviroment::~Enviroment()
{
}

NoOwnershipOf(Texture) Enviroment::getTexture() const
{
    return texture;
}

} // ns valo