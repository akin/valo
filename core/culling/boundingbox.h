
#ifndef VALO_BOUNDINGBOX_H
#define VALO_BOUNDINGBOX_H

#include <common.h>

namespace valo {

class Ray;
class BoundingBox {
public:
    glm::vec3 min;
    glm::vec3 max;

    bool intersects(const Ray& ray) const;
};

} // ns valo

#endif // VALO_BOUNDINGBOX_H
