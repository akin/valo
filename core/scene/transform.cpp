
#include "transform.h"
#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections

namespace valo {

Transform::Transform()
: scale(1.0f, 1.0f, 1.0f)
{
}

Transform::~Transform()
{
}

void Transform::detach(Transform& child)
{
    // TODO! refactor this iter horror out
    for(auto iter = children.begin() ; iter != children.end() ; ++iter) {
        if((*iter) == (&child)) {
            children.erase(iter);
            break;
        }
    }
    if(child.parent == this) {
        child.parent = nullptr;
    }
}

void Transform::detach()
{
    if(parent != nullptr) {
        parent->detach(*this);
    }
}

void Transform::attach(Transform& child)
{
    if(child.parent != nullptr) {
        child.detach();
    }
    child.parent = this;
    children.push_back(&child);
}

// Calculate world matrix, for this, and children.
void Transform::apply()
{
    // Calculate world matrix.
    glm::mat4 identity;
    glm::mat4 translationMatrix;
    glm::mat4 rotationMatrix;
    glm::mat4 scaleMatrix;

    translationMatrix = glm::translate(identity, position);
    scaleMatrix = glm::scale(identity, scale);
    rotationMatrix = glm::mat4_cast(rotation);

    glm::mat4 matrix = translationMatrix * rotationMatrix * scaleMatrix;

    if(parent != nullptr) {
        world = parent->world * matrix;
    }
    else {
        world = matrix;
    }
    for(auto child : children) {
        child->apply();
    }
    worldinverse = glm::inverse(world);
}

const glm::mat4& Transform::matrix() const
{
    return world;
}

const glm::mat4& Transform::inverseMatrix() const
{
    return worldinverse;
}

} // ns valo
