
#ifndef IMAGEBUFFER_H
#define IMAGEBUFFER_H

#include <vector>
#include "common.h"

namespace valo {

template <class TPixel>
class ImageBuffer 
{
    protected:
        std::vector<TPixel> _buffer;
        size_t _width = 0;
        size_t _height = 0;
    public:
        ImageBuffer()
        : _buffer(0)
        {
        }

        ImageBuffer(size_t width, size_t height)
        : _buffer(height * width)
        , _width(width)
        , _height(height)
        {
        }

        virtual ~ImageBuffer()
        {
        }

        void setup(size_t width, size_t height)
        {
            _buffer.resize(height * width);
            _width = width;
            _height = height;
        }

        size_t width() const
        {
            return _width;
        }

        size_t height() const
        {
            return _height;
        }

        TPixel& get(size_t x, size_t y)
        {
            return _buffer[y * _width + x];
        }

        const TPixel& getAtUv(float u, float v) const
        {
            size_t x = (u * _width);
            size_t y = (v * _height);
            return _buffer[y * _width + x];
        }

        void set(size_t x, size_t y, const TPixel& value)
        {
            _buffer[y * _width + x] = value;
        }

        void clear(const TPixel& value)
        {
            for(size_t i = 0 ; i < _buffer.size() ; ++i ) {
                _buffer[i] = value;
            }
        }

        // refactor, to get rid of this TODO!
        std::vector<TPixel>& buffer()
        {
            return _buffer;
        }

        void *raw() const
        {
            return (void*)&(_buffer[0]);
        }
};

} // ns Valo

#endif // IMAGEBUFFER_H
