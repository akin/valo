#include "gfxutil.h"
#include <stb_image.h>
#include <stb_image_write.h>
#include <functional>

namespace valo {

void convert(ImageBuffer<color::HDR_RGBA>& source, ImageBuffer<color::LDR_RGBA>& target, bool overflow)
{
    target.setup(source.width(), source.height());
    
    auto& sbuf = source.buffer();
    auto& tbuf = target.buffer();
    size_t total = source.height() * source.width();
    color::LDR_RGBA tc;
    for(size_t i = 0 ; i < total ; ++i) {
        color::HDR_RGBA& sc = sbuf[i];
        tc.r = (uint8)glm::clamp(sc.r * 255.0f, 0.0f, 255.0f);
        tc.g = (uint8)glm::clamp(sc.g * 255.0f, 0.0f, 255.0f);
        tc.b = (uint8)glm::clamp(sc.b * 255.0f, 0.0f, 255.0f);
        tc.a = (uint8)glm::clamp(sc.a * 255.0f, 0.0f, 255.0f);

        if(overflow)
        {
            tc.r = (uint8)(sc.r * 255.0f);
            tc.g = (uint8)(sc.g * 255.0f);
            tc.b = (uint8)(sc.b * 255.0f);
            tc.a = (uint8)(sc.a * 255.0f);
        }

        tbuf[i] = tc;
    }
}

void convert(ImageBuffer<color::LDR_RGBA>& source, ImageBuffer<color::HDR_RGBA>& target)
{
    target.setup(source.width(), source.height());
    
    auto& sbuf = source.buffer();
    auto& tbuf = target.buffer();
    size_t total = source.height() * source.width();
    color::HDR_RGBA tc;
    for(size_t i = 0 ; i < total ; ++i) {
        color::LDR_RGBA& sc = sbuf[i];
        tc.r = ((float)sc.r / 0xFF);
        tc.g = ((float)sc.g / 0xFF);
        tc.b = ((float)sc.b / 0xFF);
        tc.a = ((float)sc.a / 0xFF);
        tbuf[i] = tc;
    }
}

color::HDR_RGBA normalToColor(const glm::vec3& normal)
{
    return color::HDR_RGBA(
        1.0f - ((normal.x + 1.0f) * 0.5f), 
        1.0f - ((normal.y + 1.0f) * 0.5f), 
        1.0f - ((normal.z + 1.0f) * 0.5f), 
        1.0f);
}

std::map<Filetype, std::function<bool(const std::string&, const ImageBuffer<color::LDR_RGBA>&)>> ldrSaveFunctionality =
{
    {
        Filetype::png, [](const std::string& path, const ImageBuffer<color::LDR_RGBA>& source) -> bool {
            // int stbi_write_png(char const *filename, int w, int h, int comp, const void *data, int stride_in_bytes);
            int result = stbi_write_png(path.c_str(), source.width() , source.height(), 4, source.raw(), source.width() * 4);
            return result != 0;
        }
    },
    {
        Filetype::bmp, [](const std::string& path, const ImageBuffer<color::LDR_RGBA>& source) -> bool {
            // int stbi_write_bmp(char const *filename, int w, int h, int comp, const void *data)
            int result = stbi_write_bmp(path.c_str(), source.width() , source.height(), 4, source.raw());
            return result != 0;
        }
    },
    {
        Filetype::tga, [](const std::string& path, const ImageBuffer<color::LDR_RGBA>& source) -> bool {
            // int stbi_write_tga(char const *filename, int w, int h, int comp, const void *data);
            int result = stbi_write_tga(path.c_str(), source.width() , source.height(), 4, source.raw());
            return result != 0;
        }
    }
};

bool save(
    std::string path, 
    Filetype type,
    const ImageBuffer<color::LDR_RGBA>& source
    )
{
    auto iter = ldrSaveFunctionality.find(type);
    if(iter != ldrSaveFunctionality.end())
    {
        return iter->second(path, source);
    }
    return false;
}
    
bool load(
    std::string path,
    Filetype& type,
    ImageBuffer<color::LDR_RGBA>& target
    )
{
    return false;
}

} // ns valo
