#include "renderer.h"

namespace valo {
Renderer::Renderer(Scene& scene, Animation& animation, Settings& settings)
: scene(scene)
, animation(animation)
, settings(settings)
{
}

Renderer::~Renderer()
{
}

} // ns valo
