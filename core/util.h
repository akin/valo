
#ifndef CORE_UTIL_H
#define CORE_UTIL_H

#include "common.h"
#include <ray.h>

#define DPI2DPMM 0.0394f
#define DPMM2DPI 25.4f

#define M2MM 1000.0f
#define MM2M 0.001f;

// Includes the last slash
std::string getPathFromFile(const std::string& file);
std::string getFilenameFromFile(const std::string& file);

float dpi2dpmm( float dpi );
float dpmm2dpi( float dpmm );

void replaceAll(std::string& input, std::string match, std::string replacement);

size_t getCacheLineSize();

namespace valo {
    ID generateId();

    bool makePath(std::string path);
    std::string templateStr(std::string tpl, const std::map<std::string, std::string>& args);
    bool runCommand(std::string command, const std::map<std::string, std::string>& args);
    bool runCommand(std::string command);

    std::string toString(Millisecond time);
    std::string toString(Filetype format);

    std::string generateFilename(std::string filename, Millisecond time, Filetype format);

    bool resolveRaySphere( const Ray& ray , const glm::vec4& position , float radius , glm::vec4& hitpos );
}

#endif // CORE_UTIL_H
