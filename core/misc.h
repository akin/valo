
#ifndef VALO_MISC_H
#define VALO_MISC_H

#include <common.h>

namespace valo {

class OutputSetting {
public:
    std::string name;
    Filetype format;
    Millisecond step;
    std::string command;
};

class ClearSetting {
public:
    FillType fill;
    std::string node;
};

} // ns valo

#endif // VALO_MISC_H
