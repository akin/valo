
#ifndef VALO_MESH_H
#define VALO_MESH_H

#include <common.h>
#include "color.h"

namespace valo {

class Material;
class Mesh {
public:
    float32 radius = 0.0f;
    ID id;
    std::string name;
    VertexArray vertexes;
    NormalArray normals;
    WrapArray wraps;
    IndexArray indexes;
    Primitive primitive = Primitive::none;
    NoOwnershipOf(Material) material = nullptr;

    Mesh();

    friend class Culling;
};

} // ns valo

#endif // VALO_MESH_H
