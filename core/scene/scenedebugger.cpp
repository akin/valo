
#include "scenedebugger.h"
#include <logging.h>
#include <fstream>
#include <cstring>

#include "scene.h"
#include "node.h"
#include "layernode.h"
#include "cameranode.h"
#include "meshnode.h"
#include "enviromentnode.h"

#include <gfx/color.h>
#include <gfx/imagebuffer.h>
#include <gfx/mesh.h>
#include <gfx/material.h>
#include <gfx/texture.h>

#include <gfx/gfxutil.h>

#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections

namespace valo {

SceneDebugger::SceneDebugger()
{
}

SceneDebugger::~SceneDebugger()
{
}

void SceneDebugger::log(Transform &transform, size_t depth)
{
    std::string spaces(depth, ' ');
    glm::vec4 position(0.f,0.f,0.f,1.0f);
    position = transform.matrix() * position;

    LOG(logging::debug) << spaces << "- <" << transform.type << ">" << transform.id << ":" << transform.name << "@: x: " << position.x << " y: " << position.y << " z: " << position.z;

    for(auto child : transform.children) {
        log(*child, depth + 1);
    }
}

void SceneDebugger::log(Scene &scene)
{
    LOG(logging::debug) << "Scene name: " << scene._name;
    for(auto layer : scene.layers) {
        log(layer->transform, 1);
    }
}

} // ns valo