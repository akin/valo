
#ifndef VALO_APPLICATION_H
#define VALO_APPLICATION_H

#include <common.h>
#include "scene/scene.h"
#include "scene/animation.h"
#include "settings.h"

namespace valo {

class Renderer;
class Application {
private:
    std::string configFile;
    std::string logFile;
    std::string sceneFile;

    Scene scene;
    Animation animation;
    Settings settings;
    OwnershipOf(Renderer) renderer;
public:
    Application();
    ~Application();

    void setLogFile(std::string filename);
    void setConfigFile(std::string filename);

    bool configure();

    int run();
};

} // ns valo

#endif // VALO_APPLICATION_H
