
#ifndef VALO_ASSETMANAGER_H
#define VALO_ASSETMANAGER_H

#include <common.h>

namespace valo {

class Mesh;
class Material;
class Texture;
class AssetManager {
private:
    std::map<size_t, OwnershipOf(Mesh)> meshes;
    std::map<size_t, OwnershipOf(Material)> materials;
    std::map<size_t, OwnershipOf(Texture)> textures;
public:
    AssetManager();
    ~AssetManager();

    void clear();

    bool add(OwnershipOf(Mesh) item);
    bool add(OwnershipOf(Material) item);
    bool add(OwnershipOf(Texture) item);

    NoOwnershipOf(Mesh) mesh(size_t id) const;
    NoOwnershipOf(Material) material(size_t id) const;
    NoOwnershipOf(Texture) texture(size_t id) const;

    LoaderClass Loader;
};

} // ns valo

#endif // VALO_ASSETMANAGER_H
