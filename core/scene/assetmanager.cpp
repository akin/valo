
#include "assetmanager.h"
#include <gfx/mesh.h>
#include <gfx/material.h>
#include <gfx/texture.h>

namespace valo {

AssetManager::AssetManager()
{
}

AssetManager::~AssetManager()
{
    clear();
}

void AssetManager::clear()
{
    for(auto& item : meshes) {
        delete item.second;
    }
    meshes.clear();

    for(auto& item : materials) {
        delete item.second;
    }
    materials.clear();

    for(auto& item : textures) {
        delete item.second;
    }
    textures.clear();
}

bool AssetManager::add(OwnershipOf(Mesh) item)
{
    if(meshes.find(item->id) != meshes.end()) {
        return false;
    }
    meshes[item->id] = item;
    return true;
}

bool AssetManager::add(OwnershipOf(Material) item)
{
    if(materials.find(item->id) != materials.end()) {
        return false;
    }
    materials[item->id] = item;
    return true;
}

bool AssetManager::add(OwnershipOf(Texture) item)
{
    if(textures.find(item->id) != textures.end()) {
        return false;
    }
    textures[item->id] = item;
    return true;
}

NoOwnershipOf(Mesh) AssetManager::mesh(size_t id) const
{
    auto iter = meshes.find(id);
    if(iter == meshes.end()) {
        return nullptr;
    }
    return iter->second;
}

NoOwnershipOf(Material) AssetManager::material(size_t id) const
{
    auto iter = materials.find(id);
    if(iter == materials.end()) {
        return nullptr;
    }
    return iter->second;
}

NoOwnershipOf(Texture) AssetManager::texture(size_t id) const
{
    auto iter = textures.find(id);
    if(iter == textures.end()) {
        return nullptr;
    }
    return iter->second;
}

} // ns valo

