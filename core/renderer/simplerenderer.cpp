#include "simplerenderer.h"
#include <scene/scene.h>
#include <scene/animation.h>
#include <scene/node.h>
#include <scene/layernode.h>
#include <scene/cameranode.h>
#include <scene/meshnode.h>
#include <scene/enviromentnode.h>

#include <settings.h>

#include <gfx/imagebuffer.h>
#include <gfx/gfxutil.h>
#include <gfx/mesh.h>

#include <util.h>
#include <logging.h>

#include <result.h>

#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections
#include "solver.h"

#include <thread/threadpool.h>
#include <thread/shielded.h>

namespace valo {
SimpleRenderer::SimpleRenderer(Scene& scene, Animation& animation, Settings& settings)
: Renderer(scene, animation, settings)
{
}

SimpleRenderer::~SimpleRenderer()
{
}

const Enviroment& SimpleRenderer::getEnviroment(const Scene& scene, const CameraNode& camera)
{
    ClearSetting clearSetting;
    camera.get(clearSetting);

    switch(clearSetting.fill)
    {
        case FillType::enviroment :
        {
            auto enviromentNode = scene.findEnviroment(clearSetting.node);
            if(enviromentNode != nullptr) {
                return enviromentNode->getEnviroment();
            }
        }
        default:
            break;
    }

    return scene.getDefaultEnviroment();
}

void SimpleRenderer::render()
{
    LOG(logging::verbose) << "Starting render.";

    std::string outputPath = settings.getOutputPath();
    if(!makePath(outputPath)) {
        LOG(logging::error) << "Failed to create output path '" << outputPath << "', stopping rendering.";
        return;
    }

    // Ready the pools
    ThreadPool pool;

    // Warm up, cache all stuff.
    scene.apply([&](const MeshNode& node){
        node.apply([&](const Mesh& mesh){
            cache.cache(mesh);
        });
    });

    // Render..
    scene.apply([&](CameraNode& camera){
        OutputSetting outputSetting;
        ImageBuffer<color::HDR_RGBA> hdr;
        ImageBuffer<color::LDR_RGBA> ldr;

        camera.get(outputSetting);
        Transform& transform = camera.transform;

        const Enviroment& enviroment = getEnviroment(scene, camera);

        auto resolution = camera.getResolution();
        auto viewSize = camera.getViewSize();

        glm::dvec2 jump(
            viewSize.x / resolution.x, 
            viewSize.y / resolution.y);
        
        glm::dvec2 half(resolution.x * 0.5, resolution.y * 0.5);

        hdr.setup(resolution.x, resolution.y);
        ldr.setup(resolution.x, resolution.y);
        LOG(logging::verbose) << "Starting animation.";

        for(animation.reset(); !animation.finished() ; animation.advance(outputSetting.step)) {
            scene.applyTransformations();

            // Render next frame for this camera..
            std::string filename = settings.getOutputFor(generateFilename(outputSetting.name, animation.at(), outputSetting.format));

            auto projection = camera.getProjection();
            auto matrix = transform.matrix();

            auto screenToCamera = glm::inverse(projection);
            auto cameraToWorld = glm::inverse(matrix);
            auto screenToWorld = glm::inverse(matrix * projection);

            //vec3 ray_origin_wc = ( inverse(proj_mat * view_mat) * vec4(inPos+vec3(0,0,-1),1) ).xyz;

            // Do tracing
            glm::vec4 screenPos;
            Ray ray;

            Shielded<size_t> count(0);
            for(size_t y = 0 ; y < resolution.y ; ++y) {
                for(size_t x = 0 ; x < resolution.x ; ++x) {
                    screenPos.x = (-half.x + x) * jump.x;
                    screenPos.y = (-half.y + y) * jump.y;
                    screenPos.z = camera.getNear();
                    screenPos.w = 1.0;

                    // We need 2 points..
                    // the pixel position, and the pixel direction
                    // the position needs only to be translated into world position
                    // the direction is complicate,
                    //  a) directionPoint is in screenspace, we need to offset the Z axis, to create some difference
                    //     for the 2 positions we will have.. (z -= 1.0f)
                    //  b) directionPoint gets multiplied by inverse of projection, this skews the position of the
                    //     4D vector.. we could now diff the point against the screen position, to get a direction
                    //     that is in cameraSpace..
                    //  c) transform the directionPoint to worldspace
                    //  d) subtract position from directionPoint to get a difference, this diff is the direction
                    ray.position = matrix * screenPos;
                    ray.position.w = 1.0;

                    screenPos.z = camera.getNear() + 1.0f;

                    ray.direction = screenToCamera * screenPos;
                    ray.direction.w = 1.0; // W has gone BAD, fix it back to 1.0, as it is position!

                    ray.direction = (matrix * ray.direction) - ray.position;
                    ray.direction.w = 0.0; // fix W to 0.0, as it is orientation!

                    // normalize direction
                    ray.direction = glm::normalize(ray.direction);
                    
                    count.add();
                    pool.run([this, ray, enviroment, x, y, &count, &hdr]() {
                        hdr.set(
                            x, 
                            y, 
                            resolve(enviroment, ray)
                        );
                        count.remove();
                    });
                }
                count.waitUntil(0);
            }

            convert(hdr, ldr);
            if(!save(filename, outputSetting.format, ldr)) {
                LOG(logging::error) << "Failed to save frame '" << filename << "'";
            }
        }

        // Apply output command, if exists
        if(!outputSetting.command.empty()) {
            std::map<std::string, std::string> args = {
                {"path", settings.getOutputPath()},
                {"name", outputSetting.name}
            };
            runCommand(outputSetting.command, args);
        }
    });
    LOG(logging::verbose) << "Ending render.";
}

color::HDR_RGBA SimpleRenderer::resolve(const Enviroment& enviroment, const Ray& ray) const
{
    color::HDR_RGBA color(1,1,0,1);

    NoOwnershipOf(Texture) texture = enviroment.getTexture();
    if(texture != nullptr) {
        color = solveCube(*texture , ray.direction);
    }

    scene.apply([&](const MeshNode& node){
        glm::vec4 objectPosition(0,0,0,1);
        objectPosition = node.transform.matrix() * objectPosition;

        glm::vec4 hitpos(0,0,0,1);

        // Optimization, if sphere does not hit, bail out.
        if(!resolveRaySphere( 
            ray , 
            objectPosition , 
            node.transform.radius , 
            hitpos)) {
            return;
        }

        const auto& invmatrix = node.transform.inverseMatrix();
        Ray relative;
        relative.position = invmatrix * ray.position;
        relative.direction = invmatrix * ray.direction;

        glm::vec4 worldNormal;
        node.apply([&](const Mesh& mesh){
            const CullingMesh &culling = cache.get(mesh);
            
            Result result;
            if(!culling.solve(relative, result)) {
                return;
            }
            
            if(result.hit) {
                color = color::HDR_RGBA(
                    0.2f,
                    0.2f,
                    0.2f ,
                    1.0f);
                
                if(texture != nullptr) {
                    // from local normal to world normal
                    worldNormal = node.transform.matrix() * result.normal;
                    worldNormal.w = 0.0f;
                    color::HDR_RGBA altcolor = solveCube(*texture , worldNormal);

                    color.r += altcolor.r;
                    color.g += altcolor.g;
                    color.b += altcolor.b;
                }
            }
        });
    });

    return color;
}

} // ns valo
