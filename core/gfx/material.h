
#ifndef VALO_MATERIAL_H
#define VALO_MATERIAL_H

#include <common.h>
#include "color.h"

namespace valo {

class Material {
public:
    size_t id;
    std::string name;

    Material();
    virtual ~Material();
    virtual color::RGBA solve(const glm::vec3& position) const;
};

} // ns valo

#endif // VALO_MATERIAL_H
