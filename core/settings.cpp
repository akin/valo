#include "settings.h"

namespace valo {

std::string Settings::getOutputFor(std::string path) const
{
    return getOutputPath() + path;
}

std::string Settings::getOutputPath() const
{
    return rootPath + outputPath;
}

} // ns valo