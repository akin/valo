
#ifndef VALO_JSONTOOL_H
#define VALO_JSONTOOL_H

#include <common.h>
#include <json.hpp>
#include "misc.h"

namespace valo {
using json = nlohmann::json;

bool read(const json& root, const char *key, std::string& out);
bool read(const json& root, const char *key, int& out);
bool read(const json& root, const char *key, int64& out);
bool read(const json& root, const char *key, float& out);
bool read(const json& root, const char *key, double& out);
bool read(const json& root, const char *key, Filetype& out);

// complex
bool read(const json& root, const char *key, OutputSetting& out);
bool read(const json& root, const char *key, glm::vec2& out);
bool read(const json& root, const char *key, glm::vec3& out);
bool read(const json& root, const char *key, glm::vec4& out);

bool read(const json& root, const char *key, glm::ivec2& out);
bool read(const json& root, const char *key, glm::ivec3& out);
bool read(const json& root, const char *key, glm::ivec4& out);

bool read(const json& root, const char *key, glm::dvec2& out);
bool read(const json& root, const char *key, glm::dvec3& out);
bool read(const json& root, const char *key, glm::dvec4& out);

bool readDimensions(const json& root, const char *key, glm::vec2& out);
bool readDimensions(const json& root, const char *key, glm::ivec2& out);
bool readDimensions(const json& root, const char *key, glm::dvec2& out);

} // ns valo

#endif // VALO_JSONTOOL_H
