
#ifndef VALO_ANIMATION_H
#define VALO_ANIMATION_H

#include <common.h>

namespace valo {

class Scene;
class Animation {
protected:
    Scene& scene;
    Millisecond position;
    Millisecond end;
public:
    Animation(Scene& scene);
    ~Animation();

    void reset();

    void setDuration(Millisecond duration);

    Millisecond length() const;
    Millisecond at() const;
    bool finished() const;

    void apply();

    void seek(Millisecond position);
    void advance(Millisecond amount);

    LoaderClass Loader;
};

} // ns valo

#endif // VALO_ANIMATION_H
