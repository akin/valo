
#ifndef VALO_RENDERER_H
#define VALO_RENDERER_H

#include <common.h>

namespace valo {

class Scene;
class Animation;
class Settings;
class Renderer {
protected:
    Scene& scene;
    Animation& animation;
    Settings& settings;
public:
    Renderer(Scene& scene, Animation& animation, Settings& settings);
    virtual ~Renderer();

    virtual void render() = 0;
};

} // ns valo

#endif // VALO_RENDERER_H
