
#include "meshnode.h"
#include <gfx/mesh.h>

namespace valo {

MeshNode::MeshNode()
{
}

MeshNode::~MeshNode()
{
}

void MeshNode::apply(std::function<void(Mesh&)> func)
{
    for(auto item : meshes) {
        func(*item);
    }
}

void MeshNode::apply(std::function<void(const Mesh&)> func) const
{
    for(auto item : meshes) {
        func(*item);
    }
}

} // ns valo