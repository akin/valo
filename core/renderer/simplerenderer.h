
#ifndef VALO_SIMPLERENDERER_H
#define VALO_SIMPLERENDERER_H

#include "renderer.h"
#include <gfx/color.h>
#include <ray.h>
#include "cache.h"

namespace valo {

class CameraNode;
class Enviroment;
class SimpleRenderer : public Renderer {
protected:
    Cache cache;
protected:
    const Enviroment& getEnviroment(const Scene& scene, const CameraNode& camera);
    color::HDR_RGBA resolve(const Enviroment& enviroment, const Ray& ray) const;
public:
    SimpleRenderer(Scene& scene, Animation& animation, Settings& settings);
    virtual ~SimpleRenderer();

    virtual void render();
};

} // ns valo

#endif // VALO_SIMPLERENDERER_H
