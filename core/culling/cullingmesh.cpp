
#include "cullingmesh.h"
#include <gfx/mesh.h>
#include <ray.h>
#include <result.h>
#include "boundingbox.h"

// Triangle intersectiontest
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>

namespace valo {

class CullingNode : public BoundingBox {
public:
    enum class Type {
        Node = 0,
        Leaf = 1
    };

    Type type = Type::Node;
public:
    CullingNode();
    ~CullingNode();

    std::vector<CullingNode*> childs;
    std::vector<Index> primitives;
};

CullingNode::CullingNode()
{
}

CullingNode::~CullingNode()
{
    for(auto child : childs) {
        delete child;
    }
    childs.clear();
}

CullingMesh::CullingMesh()
: node(nullptr)
, mesh(nullptr)
{
}

CullingMesh::~CullingMesh()
{
    clear();
}

void CullingMesh::clear()
{
    delete node;
    node = nullptr;
}

bool setup(CullingNode& item, const Mesh& mesh, const std::vector<Index>& primitives)
{
    // The child will be a leaf, if it has less or eq than 10 primitives
    // If it has 0 primitives, the child should be deleted.

    // If there are more than 10 primitives, make it a Node, and request a division, and apply.
}

void applyNode(CullingNode& parent, const Mesh& mesh, const std::vector<Index>& primitives)
{
    // Lets try to divide into 8 (octree!)
    // The child will be a leaf, if it has 10 primitives

    // Create node, if setup accepts the node push the node into parent.

    // divide
}

void CullingMesh::build(const Mesh& mesh)
{
    if(node != nullptr) {
        clear();
    }
    node = new CullingNode();

    // The mesh is at the center..
    node->min.x = -mesh.radius;
    node->min.y = -mesh.radius;
    node->min.z = -mesh.radius;
    node->max.x = mesh.radius;
    node->max.y = mesh.radius;
    node->max.z = mesh.radius;

    // Now, divide the node into smaller pieces.
    std::vector<Index> primitives;
    size_t total = mesh.indexes.size() / 3;
    primitives.reserve(total);
    for(Index index = 0 ; index < total ; ++index)
    {
        primitives.push_back(index);
    }
    applyNode(*node, mesh, primitives);

    // HAXXX
    this->mesh = &mesh;
}

bool CullingMesh::solve(const Ray& ray, Result& result) const
{
    // Bruteforce triangle detection..
    glm::vec3 position(ray.position);
    glm::vec3 direction(ray.direction);
    glm::vec3 hitPosition;

    result.hit = false;

    bool hit = false;
    const Mesh& mesh = *(this->mesh);
    double distance = 0.0f;
    for(Index index = 0 ; (index + 2) < mesh.indexes.size() ; index += 3)
    {
        glm::vec3 const &a = mesh.vertexes[mesh.indexes[index + 0]];
        glm::vec3 const &b = mesh.vertexes[mesh.indexes[index + 1]];
        glm::vec3 const &c = mesh.vertexes[mesh.indexes[index + 2]];

        hit = glm::intersectLineTriangle(
            position, 
            direction, 
            a,
            b,
            c,
            hitPosition);

        distance = glm::distance(position, hitPosition);
        
        if(hit && ((!result.hit) || result.distance > distance)) {
            result.index = index / 3;
            result.position = glm::vec4(hitPosition, 1.0f);
            result.normal = glm::vec4(glm::normalize(glm::cross(c - a, b - a)), 0.0f);
            result.distance = distance;
            result.hit = true;
        }
    }
    return result.hit;
}

} // ns valo
