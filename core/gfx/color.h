
#ifndef VALO_COLOR_H
#define VALO_COLOR_H

#include <types.h>
#include "tcolor.h"

namespace valo {
    namespace color {
        using LDR_RGB = TColor3<uint8>;
        using LDR_RGBA = TColor4<uint8>;

        using HDR_RGB = TColor3<float32>;
        using HDR_RGBA = TColor4<float32>;

        using RGB = HDR_RGB;
        using RGBA = HDR_RGBA;
    }
}

#endif // VALO_COLOR_H
