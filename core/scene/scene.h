
#ifndef VALO_SCENE_H
#define VALO_SCENE_H

#include <common.h>
#include <functional>
#include "assetmanager.h"
#include "enviroment.h"

namespace valo {

class Node;
class LayerNode;
class MeshNode;
class CameraNode;
class EnviromentNode;
class Scene {
private:
    AssetManager assets;
    std::vector<OwnershipOf(Node)> nodes;
    std::vector<OwnershipOf(LayerNode)> layers;
    std::vector<OwnershipOf(MeshNode)> meshes;
    std::vector<OwnershipOf(CameraNode)> cameras;
    std::vector<OwnershipOf(EnviromentNode)> enviroments;

    Enviroment defaultEnviroment;
private:
    std::string _name;

    void init();
public:
    Scene();
    ~Scene();

    std::string name() const;

    void clear();
    NoOwnershipOf(LayerNode) getDefaultLayer() const;

    void apply(std::function<void(CameraNode&)> func);
    void apply(std::function<void(MeshNode&)> func);
    void apply(std::function<void(const MeshNode&)> func) const;

    NoOwnershipOf(EnviromentNode) findEnviroment(std::string name) const;

    const Enviroment& getDefaultEnviroment() const;

    void applyTransformations();

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_SCENE_H
