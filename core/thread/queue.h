/**
 * Thread safe Queue.
 */
#ifndef VALO_QUEUE
#define VALO_QUEUE
 
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <utility>
 
namespace valo {

template <typename T>
class Queue
{
private:
    std::atomic_bool valid{true};
    mutable std::mutex mutex;
    std::queue<T> queue;
    std::condition_variable condition;
public:
    ~Queue()
    {
        invalidate();
    }

    /**
    * Attempt to get the first value in the queue.
    * Returns true if a value was successfully written to the out parameter, false otherwise.
    */
    bool tryPop(T& out)
    {
        std::lock_guard<std::mutex> lock{mutex};
        if(queue.empty() || !valid)
        {
            return false;
        }
        out = std::move(queue.front());
        queue.pop();
        return true;
    }

    /**
    * Get the first value in the queue.
    * Will block until a value is available unless clear is called or the instance is destructed.
    * Returns true if a value was successfully written to the out parameter, false otherwise.
    */
    bool waitPop(T& out)
    {
        std::unique_lock<std::mutex> lock{mutex};
        condition.wait(lock, [this]()
        {
            return !queue.empty() || !valid;
        });
        /*
        * Using the condition in the predicate ensures that spurious wakeups with a valid
        * but empty queue will not proceed, so only need to check for validity before proceeding.
        */
        if(!valid)
        {
            return false;
        }
        out = std::move(queue.front());
        queue.pop();
        return true;
    }

    void push(T value)
    {
        std::lock_guard<std::mutex> lock{mutex};
        queue.push(std::move(value));
        condition.notify_one();
    }

    bool empty() const
    {
        std::lock_guard<std::mutex> lock{mutex};
        return queue.empty();
    }

    size_t size() const
    {
        std::lock_guard<std::mutex> lock{mutex};
        return queue.size();
    }

    void clear()
    {
        std::lock_guard<std::mutex> lock{mutex};
        while(!queue.empty())
        {
            queue.pop();
        }
        condition.notify_all();
    }

    /**
    * Invalidate the queue.
    * Used to ensure no conditions are being waited on in waitPop when
    * a thread or the application is trying to exit.
    * The queue is invalid after calling this method and it is an error
    * to continue using a queue after this method has been called.
    */
    void invalidate()
    {
        std::lock_guard<std::mutex> lock{mutex};
        valid = false;
        condition.notify_all();
    }

    /**
    * Returns whether or not this queue is valid.
    */
    bool isValid() const
    {
        std::lock_guard<std::mutex> lock{mutex};
        return valid;
    }
};

} // ns valo
 
#endif