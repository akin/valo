
#include "loader.h"
#include <logging.h>
#include <jsontool.h>
#include <fstream>
#include <cstring>

#include <stb_image.h>

#include <scene/scene.h>
#include <scene/node.h>
#include <scene/layernode.h>
#include <scene/cameranode.h>
#include <scene/meshnode.h>
#include <scene/enviromentnode.h>

#include <gfx/color.h>
#include <gfx/imagebuffer.h>
#include <gfx/mesh.h>
#include <gfx/material.h>
#include <gfx/texture.h>

#include <gfx/gfxutil.h>

#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections

namespace valo {

void convert(const aiMatrix4x4& in, glm::mat4& out)
{
    out[0][0] = in.a1;
    out[0][1] = in.b1;
    out[0][2] = in.c1;
    out[0][3] = in.d1;
    out[1][0] = in.a2;
    out[1][1] = in.b2;
    out[1][2] = in.c2;
    out[1][3] = in.d2;
    out[2][0] = in.a3;
    out[2][1] = in.b3;
    out[2][2] = in.c3;
    out[2][3] = in.d3;
    out[3][0] = in.a4;
    out[3][1] = in.b4;
    out[3][2] = in.c4;
    out[3][3] = in.d4;
}

Loader::Loader(Scene& scene, Animation& animation)
: scene(scene)
, animation(animation)
{
}

Loader::~Loader()
{
}

void Loader::clear()
{
    localTextureIdMap.clear();
    localMaterialIdMap.clear();
    localMeshIdMap.clear();
}

void Loader::setRoot(std::string root)
{
    this->root = root;
}

OwnershipOf(Texture) Loader::loadTexture(std::string filename)
{
    LOG(logging::verbose) << "Loading image file (" << filename << ")";

    OwnershipOf(Texture) texture = new Texture;

    // Packed
    int x,y,components;
    
    if(stbi_info(filename.c_str(), &x, &y, &components) == 0) {
        delete texture;
        LOG(logging::error) << "Could not decode image file info (" << filename << ")[" << stbi_failure_reason() << "]";
        return nullptr;
    }

    unsigned char *data = stbi_load(filename.c_str(), &x, &y, &components, 4);
    if(data == nullptr) {
        delete texture;
        LOG(logging::error) << "Could not decode image file (" << filename << ")[" << stbi_failure_reason() << "]";
        return nullptr;
    }

    ImageBuffer<color::LDR_RGBA> buffer;
    buffer.setup(x, y);
    auto& buf = buffer.buffer();

    std::memcpy(&(buf[0]), data, buf.size() * sizeof(color::LDR_RGBA));
    stbi_image_free(data);

    convert(buffer, *texture);

    return texture;
}

bool Loader::importTextures(const aiScene *aiscene)
{
    if(!aiscene->HasTextures()) {
        return true;
    }

    for(size_t textureIdx = 0 ; textureIdx < aiscene->mNumTextures ; ++textureIdx) {
        const aiTexture* aitexture = aiscene->mTextures[textureIdx];
        OwnershipOf(Texture) texture = new Texture;

        if(aitexture->mHeight != 0 ) {
            // Uncompressed
            ImageBuffer<color::LDR_RGBA> buffer;
            buffer.setup(aitexture->mWidth, aitexture->mHeight);
            auto& buf = buffer.buffer();
            size_t total = buffer.height() * buffer.width();
            color::LDR_RGBA ldr;
            for(size_t i = 0; i < total; ++i) {
                const aiTexel& texel = aitexture->pcData[i];
                ldr.r = texel.r;
                ldr.g = texel.g;
                ldr.b = texel.b;
                ldr.a = texel.a;
                buf[i] = ldr;
            }

            convert(buffer, *texture);
        }
        else {
            // Packed
            int x,y,components;
            
            if(stbi_info_from_memory((const unsigned char *)aitexture->pcData, aitexture->mWidth, &x, &y, &components) == 0) {
                delete texture;
                LOG(logging::error) << "Could not decode embedded image file info (" << aitexture->achFormatHint << ")";
                continue;
            }

            unsigned char *data = stbi_load_from_memory((const unsigned char *)aitexture->pcData, aitexture->mWidth, &x, &y, &components, 4);
            if(data == nullptr) {
                delete texture;
                LOG(logging::error) << "Could not decode embedded image file (" << aitexture->achFormatHint << ")";
                continue;
            }

            ImageBuffer<color::LDR_RGBA> buffer;
            buffer.setup(x, y);
            auto& buf = buffer.buffer();

            std::memcpy(&(buf[0]), data, buf.size() * sizeof(color::LDR_RGBA));
            stbi_image_free(data);

            convert(buffer, *texture);
        }

        scene.assets.add(texture);
        localTextureIdMap[textureIdx] = texture->id;
    }
    return true;
}

bool Loader::importMaterials(const aiScene *aiscene)
{
    if(!aiscene->HasMaterials()) {
        return true;
    }

    for(size_t materialIdx = 0 ; materialIdx < aiscene->mNumMaterials ; ++materialIdx) {
        const aiMaterial* aimaterial = aiscene->mMaterials[materialIdx]; 
        OwnershipOf(Material) material = new Material;
        /*
        if(aimaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            aiString Path;

            if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
                std::string FullPath = Dir + "/" + Path.data;
                m_Textures[i] = new Texture(GL_TEXTURE_2D, FullPath.c_str());

                if (!m_Textures[i]->Load()) {
                    printf("Error loading texture '%s'\n", FullPath.c_str());
                    delete m_Textures[i];
                    m_Textures[i] = NULL;
                    Ret = false;
                }
            }
        }
        */
        /*
        for(size_t propertyIdx = 0; propertyIdx < aimaterial->mNumProperties ; ++propertyIdx) {
            aiMaterialProperty *aiproperty = aimaterial->mProperties[propertyIdx];
        }
        */
        scene.assets.add(material);
        localMaterialIdMap[materialIdx] = material->id;
    }
    return true;
}

bool Loader::importMeshes(const aiScene *aiscene)
{
    if(!aiscene->HasMeshes()) {
        return true;
    }

    for(size_t meshIdx = 0 ; meshIdx < aiscene->mNumMeshes ; ++meshIdx) {
        aiMesh* aimesh = aiscene->mMeshes[meshIdx]; 

        OwnershipOf(Mesh) mesh = new Mesh;
        mesh->name = aimesh->mName.C_Str();
        mesh->primitive = Primitive::triangle;

        // Bind material
        {
            auto iter = localMaterialIdMap.find(aimesh->mMaterialIndex);
            if(iter != localMaterialIdMap.end()) {
                mesh->material = scene.assets.material(iter->second);
            }
            else {
                mesh->material = nullptr;
            }
        }

        // Reserve some memory..
        size_t sizes = aimesh->mNumVertices;
        if(aimesh->HasPositions()) {
            mesh->vertexes.reserve(sizes);
            float distance = 0.0f;
            glm::vec3 position;
            for(size_t i = 0 ; i < sizes ; ++i) {
                aiVector3D& pos = aimesh->mVertices[i];
                position = glm::vec3(pos.x,pos.y,pos.z);
                mesh->vertexes.emplace_back(position);

                distance = glm::length(position);
                if(distance > mesh->radius) {
                    mesh->radius = distance;
                }
            }
        }
        if(aimesh->HasNormals()) {
            mesh->normals.reserve(sizes);
            for(size_t i = 0 ; i < sizes ; ++i) {
                aiVector3D& pos = aimesh->mNormals[i]; 
                mesh->normals.emplace_back(pos.x,pos.y,pos.z);
            }
        }
        if(aimesh->HasTextureCoords(0)) {
            mesh->wraps.reserve(sizes);
            for(size_t i = 0 ; i < sizes ; ++i) {
                aiVector3D& pos = aimesh->mTextureCoords[0][i];
                mesh->wraps.emplace_back(pos.x,pos.y,pos.z);
            }
        }
        // !TODO!
        // Multitexturing!
        // VertexColors!
        // Bones!

        // Indices..
        mesh->indexes.reserve(aimesh->mNumFaces * 3);
        for(size_t faceIdx = 0 ; faceIdx < aimesh->mNumFaces ; ++faceIdx) {
            const aiFace& aiface = aimesh->mFaces[faceIdx];
            for(size_t i = 0; i < 3; ++i) {
                uint indice = aiface.mIndices[i];
                mesh->indexes.push_back(indice);
            }
        }

        scene.assets.add(mesh);
        localMeshIdMap[meshIdx] = mesh->id;
    }
    return true;
}

void Loader::parseNodes(const aiNode *ainode, Transform& parent) 
{
    if(ainode == nullptr) {
        return;
    }

    OwnershipOf(Node) self = nullptr;
    if(ainode->mNumMeshes > 0)
    {
        OwnershipOf(MeshNode) meshnode = new MeshNode;
        meshnode->transform.type = "mesh";

        for(size_t i = 0 ; i < ainode->mNumMeshes ; ++i) {
            size_t index = ainode->mMeshes[i];
            auto iter = localMeshIdMap.find(index);
            if(iter == localMeshIdMap.end()) {
                LOG(logging::error) << "Error, could not find mesh (" << index << "): node creation failed.";
                continue;
            }
            NoOwnershipOf(Mesh) mesh = scene.assets.mesh(iter->second);
            meshnode->meshes.push_back(mesh);
            if(meshnode->transform.radius < mesh->radius) {
                meshnode->transform.radius = mesh->radius;
            }
        }

        self = meshnode;
        scene.meshes.push_back(meshnode);
    }
    else
    {
        self = new Node;
        self->transform.type = "node";
        scene.nodes.push_back(self);
    }

    Transform& transform = self->transform;
    parent.attach(transform);

    aiVector3t<float> scaling;
    aiQuaterniont<float> rotation;
    aiVector3t<float> position;

    //convert(ainode->mTransformation, transform.matrix);
    ainode->mTransformation.Decompose(scaling, rotation, position);
    
    transform.position = glm::vec3(position.x, position.y, position.z);
    transform.scale = glm::vec3(scaling.x, scaling.y, scaling.z);
    transform.rotation = glm::quat(rotation.w, rotation.x, rotation.y, rotation.z);

    transform.name = ainode->mName.C_Str();
    
    for(size_t i = 0 ; i < ainode->mNumChildren ; ++i) {
        parseNodes(ainode->mChildren[i], transform);
    }
}

bool Loader::load(const std::string path) 
{
    //check if file exists
    {
        std::ifstream fin(path.c_str());
        if(fin.fail()) {
            LOG(logging::error) << "Could not open file (" << path << ")";
            return false;
        }
    }

    LOG(logging::verbose) << "Assimp Import scene '" << path << "'.";
    Assimp::Importer importer; 
    const aiScene* aiscene = importer.ReadFile( 
        path,  
        aiProcess_CalcTangentSpace       |  
        aiProcess_Triangulate            | // Make faces triangles
        aiProcess_JoinIdenticalVertices  | 
        aiProcess_SortByPType);

    if(!aiscene) {
        LOG(logging::error) << "Error parsing (" << path << "): " << importer.GetErrorString();
        return false;
    }

    /*
    scene.clear();
    scene._name = path;
    */

    // Import textures
    LOG(logging::verbose) << "Assimp Import textures.";
    if(!importTextures(aiscene)) {
        LOG(logging::error) << "Error importing (" << path << "): texture import failed.";
        return false;
    }

    // Import materials
    LOG(logging::verbose) << "Assimp Import materials.";
    if(!importMaterials(aiscene)) {
        LOG(logging::error) << "Error importing (" << path << "): material import failed.";
        return false;
    }

    // Import meshes
    LOG(logging::verbose) << "Assimp Import meshes.";
    if(!importMeshes(aiscene)) {
        LOG(logging::error) << "Error importing (" << path << "): mesh import failed.";
        return false;
    }

    // Load scene itself (cameras, nodes etc)
    // everything is loaded to a new layer.
    LOG(logging::verbose) << "Assimp create layer.";
    OwnershipOf(LayerNode) layer = new LayerNode;
    layer->transform.type = "layer";
    scene.layers.push_back(layer);
    layer->transform.name = path;

    LOG(logging::verbose) << "Assimp import nodes.";
    parseNodes(aiscene->mRootNode, layer->transform);

    LOG(logging::verbose) << "Assimp import cameras.";
    for(size_t i = 0 ; i < aiscene->mNumCameras ; ++i) {
        const aiCamera *aicamera = aiscene->mCameras[i];
        OwnershipOf(CameraNode) camera = new CameraNode;
        
        Transform& transform = camera->transform;
        transform.name = aicamera->mName.C_Str();

        glm::vec3 position(aicamera->mPosition.x, aicamera->mPosition.y, aicamera->mPosition.z);
        glm::vec3 lookAt(aicamera->mLookAt.x, aicamera->mLookAt.y, aicamera->mLookAt.z);
        glm::vec3 up(aicamera->mUp.x, aicamera->mUp.y, aicamera->mUp.z);

        transform.position = position;

        LOG(logging::verbose) << "Assimp Camera Quaternion rotation TODO!";
        //transform.matrix = glm::lookAt(position, lookAt, up);

        camera->projection = glm::perspective(
            aicamera->mHorizontalFOV, 
            aicamera->mAspect, 
            aicamera->mClipPlaneNear, 
            aicamera->mClipPlaneFar);

        scene.cameras.push_back(camera);
        layer->transform.attach(camera->transform);
    }

    LOG(logging::verbose) << "Assimp parseLights. TODO!";
    LOG(logging::verbose) << "Assimp parseAnimations. TODO!";
 
    clear();
    LOG(logging::verbose) << "Assimp done.";

    // We're done. Everything will be cleaned up by the importer destructor
    return true;
}

bool Loader::load(const json& node)
{
    std::string type;
    if(!read(node,"type", type)) {
        LOG(logging::error) << "Type required in json node.";
        return false;
    }

    if(type == "camera") {
        return loadCamera(node);
    }
    if(type == "enviroment") {
        return loadEnviroment(node);
    }

    LOG(logging::error) << "Unkown type in json node ''" << type << "'.";
    return false;
}

bool Loader::loadTransform(const json& node, Transform& transform)
{
    // Position
    // Scale
    // Rotation
    read(node,"position",transform.position);
    read(node,"scale",transform.scale);
    LOG(logging::verbose) << "Json Camera Quaternion rotation TODO!";
    
    /*
    glm::vec3 position(aicamera->mPosition.x, aicamera->mPosition.y, aicamera->mPosition.z);
    glm::vec3 lookAt(aicamera->mLookAt.x, aicamera->mLookAt.y, aicamera->mLookAt.z);
    glm::vec3 up(aicamera->mUp.x, aicamera->mUp.y, aicamera->mUp.z);

    transform.matrix = glm::lookAt(position, lookAt, up);

    camera->projection = glm::perspective(
        aicamera->mHorizontalFOV, 
        aicamera->mAspect, 
        aicamera->mClipPlaneNear, 
        aicamera->mClipPlaneFar);
    */
    return true;
}

bool Loader::loadProjection(const json& node, glm::mat4& projection, glm::dvec2& viewSize, float& min, float& max)
{
    std::string type;
    if(!(read(node,"type",type))) {
        LOG(logging::error) << "Type required in json node.";
        return false;
    }

    if(type == "perspective") {
        LOG(logging::verbose) << "Perspective camera.";
        float width,height,fov;
        if(!(read(node,"near", min) && 
            read(node,"far", max) && 
            read(node,"width", width) && 
            read(node,"height", height) && 
            read(node,"fov", fov))) {
            LOG(logging::error) << "Missing field for ''" << type << "'.";
            return false;
        }
        if(width == 0.0f || height == 0.0f) {
            LOG(logging::error) << "Projection can not have width or height as 0.";
            return false;
        }
        projection = glm::perspective(
            glm::radians(fov),
            height / width,
            min,
            max
        );
        viewSize.x = width;
        viewSize.y = height;
    }
    else if(type == "frustum") {
        LOG(logging::verbose) << "Perspective camera.";
        float width,height;
        if(!(read(node,"near", min) && 
            read(node,"far", max) && 
            read(node,"width", width) && 
            read(node,"height", height))) {
            LOG(logging::error) << "Missing field for ''" << type << "'.";
            return false;
        }
        if(width == 0.0f || height == 0.0f) {
            LOG(logging::error) << "Projection can not have width or height as 0.";
            return false;
        }
        // sigh.. so units are in 1x1 cube int the world..
        float halfW =  0.5f;
        float halfH =  0.5f;
        projection = glm::frustum(
            -halfW,
            halfW,
            -halfH,
            halfH,
            min,
            max);
        viewSize.x = width;
        viewSize.y = height;
    }
    else if(type == "orthogonal") {
        LOG(logging::verbose) << "Orthogonal camera.";
        float width,height,fov;
        if(!(read(node,"near", min) && 
            read(node,"far", max) && 
            read(node,"width", width) && 
            read(node,"height", height) && 
            read(node,"fov", fov))) {
            LOG(logging::error) << "Missing field for ''" << type << "'.";
            return false;
        }
        if(width == 0.0f || height == 0.0f) {
            LOG(logging::error) << "Projection can not have width or height as 0.";
            return false;
        }
        float halfW = width * 0.5f;
        float halfH = height * 0.5f;
        projection = glm::ortho(
            -halfW,
            halfW,
            -halfH,
            halfH,
            min,
            max
        );

        viewSize.x = width;
        viewSize.y = height;
    }
    else {
        LOG(logging::error) << "Unkown type in json projection node ''" << type << "'.";
        return false;
    }
    return true;
}

bool Loader::loadCamera(const json& node)
{
    LOG(logging::verbose) << "Json import camera.";

    OwnershipOf(CameraNode) camera = new CameraNode;
    
    Transform& transform = camera->transform;
    read(node,"name", transform.name);
    read(node,"output", camera->output);
    readDimensions(node,"resolution", camera->resolution);

    {
        auto iter = node.find("transform");
        if(iter != node.end()) {
            loadTransform(iter.value(), transform);
        }
    }
    {
        auto iter = node.find("projection");
        if(iter != node.end()) {
            loadProjection(
                iter.value(), 
                camera->projection,
                camera->viewSize,
                camera->min,
                camera->max);
        }
    }
    {
        camera->clear.fill = FillType::none;
        
        auto iter = node.find("clear");
        if(iter != node.end()) {
            std::string type;
            std::string name;

            read(iter.value(),"type", type);
            read(iter.value(),"name", name);

            if(type == "enviroment") {
                camera->clear.fill = FillType::enviroment;
                camera->clear.node = name;
            }
        }
    }

    scene.cameras.push_back(camera);
    scene.getDefaultLayer()->transform.attach(transform);
    return true;
}

bool Loader::loadEnviroment(const json& node)
{
    LOG(logging::verbose) << "Json import Enviroment.";

    OwnershipOf(EnviromentNode) envnode = new EnviromentNode;
    
    Transform& transform = envnode->transform;
    read(node,"name", transform.name);
    read(node,"src", envnode->enviroment.source);

    OwnershipOf(Texture) texture = loadTexture(root + envnode->enviroment.source);

    if(texture != nullptr) {
        scene.assets.add(texture);
        envnode->enviroment.texture = texture;
    }

    scene.enviroments.push_back(envnode);
    scene.getDefaultLayer()->transform.attach(transform);
    return true;
}

} // ns valo