
#ifndef COMMON_COMMON_H
#define COMMON_COMMON_H

#include <string>
#include <glm/glm.hpp>
#include "types.h"
#include "gfx/color.h"

namespace valo {

using VertexArray = std::vector<glm::vec3>;
using NormalArray = std::vector<glm::vec3>;
using WrapArray = std::vector<glm::vec3>;
using IndexArray = std::vector<Index>;

}; // ns valo

#endif // COMMON_COMMON_H
