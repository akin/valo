
#include "cache.h"
#include <util.h>

namespace valo {

Cache::Cache()
{
}

Cache::~Cache()
{
}

void Cache::clear()
{
    meshCache.clear();
}

void Cache::cache(const Mesh& mesh)
{
    if(meshCache.find(mesh.id) != meshCache.end()) {
        return;
    }
    meshCache[mesh.id].build(mesh);
}

const CullingMesh& Cache::get(const Mesh& mesh) const
{
    auto iter = meshCache.find(mesh.id);
    if(iter == meshCache.end()) {
        // Exception!
    }
    return iter->second;
}

} // ns valo

