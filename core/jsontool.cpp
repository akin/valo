
#include "jsontool.h"

namespace valo {

bool read(const json& root, const char *key, std::string& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_string()) {
        return false;
    }
    out = node.get<std::string>();
    return true;
}

bool read(const json& root, const char *key, int& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_number()) {
        return false;
    }
    out = node.get<int>();
    return true;
}

bool read(const json& root, const char *key, int64& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_number()) {
        return false;
    }
    out = node.get<int64>();
    return true;
}

bool read(const json& root, const char *key, float& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_number()) {
        return false;
    }
    out = node.get<float>();
    return true;
}

bool read(const json& root, const char *key, double& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_number()) {
        return false;
    }
    out = node.get<double>();
    return true;
}

bool read(const json& root, const char *key, Filetype& out)
{
    std::string value;
    if(!read(root, key, value)) {
        return false;
    }
    out = Filetype::none;

    if(value == "png") out = Filetype::png;
    if(value == "bmp") out = Filetype::bmp;
    if(value == "tif") out = Filetype::tif;
    if(value == "hdr") out = Filetype::hdr;
    if(value == "obj") out = Filetype::obj;
    if(value == "json") out = Filetype::json;
    if(value == "txt") out = Filetype::txt;

    return true;
}

// complex
bool read(const json& root, const char *key, OutputSetting& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "name", out.name);
    read(node, "format", out.format);
    read(node, "stepms", out.step);
    read(node, "command", out.command);

    return true;
}

bool read(const json& root, const char *key, glm::vec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    return true;
}

bool read(const json& root, const char *key, glm::vec3& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    return true;
}

bool read(const json& root, const char *key, glm::vec4& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    read(node, "w", out.w);
    return true;
}

bool read(const json& root, const char *key, glm::ivec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    return true;
}

bool read(const json& root, const char *key, glm::ivec3& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    return true;
}

bool read(const json& root, const char *key, glm::ivec4& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    read(node, "w", out.w);
    return true;
}

bool read(const json& root, const char *key, glm::dvec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    return true;
}

bool read(const json& root, const char *key, glm::dvec3& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    return true;
}

bool read(const json& root, const char *key, glm::dvec4& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "x", out.x);
    read(node, "y", out.y);
    read(node, "z", out.z);
    read(node, "w", out.w);
    return true;
}

bool readDimensions(const json& root, const char *key, glm::vec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "width", out.x);
    read(node, "height", out.y);
    return true;
}

bool readDimensions(const json& root, const char *key, glm::ivec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "width", out.x);
    read(node, "height", out.y);
    return true;
}

bool readDimensions(const json& root, const char *key, glm::dvec2& out)
{
    auto iter = root.find(key);
    if(iter == root.end()) {
        return false;
    }
    auto& node = iter.value();
    if(!node.is_object()) {
        return false;
    }

    read(node, "width", out.x);
    read(node, "height", out.y);
    return true;
}

} // ns valo
