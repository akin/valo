
#ifndef VALO_ENVIROMENT_H
#define VALO_ENVIROMENT_H

#include <common.h>

namespace valo {

class Texture;
class Enviroment {
protected:
    std::string source;
    NoOwnershipOf(Texture) texture;
public:
    Enviroment();
    ~Enviroment();

    NoOwnershipOf(Texture) getTexture() const;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_ENVIROMENT_H
