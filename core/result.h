#ifndef VALO_RESULT_H
#define VALO_RESULT_H

#include <common.h>

namespace valo {

class Result {
public:
    glm::vec4 position;
    glm::vec4 normal;
    double distance;
    Index index;
    bool hit;
};

} // ns valo

#endif // VALO_RESULT_H
