
#ifndef VALO_TEXTURE_H
#define VALO_TEXTURE_H

#include "imagebuffer.h"
#include "color.h"

namespace valo {
class Texture : public ImageBuffer<color::HDR_RGBA>
{
public:
    size_t id;
    std::string name;

    Texture();
};

} // ns valo

#endif // VALO_TEXTURE_H
