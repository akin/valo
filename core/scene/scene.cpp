
#include "scene.h"
#include "node.h"
#include "layernode.h"
#include "meshnode.h"
#include "cameranode.h"
#include "enviromentnode.h"

namespace valo {

Scene::Scene()
{
    init();
}

Scene::~Scene()
{
    clear();

    for(auto& item : layers) {
        delete item;
    }
    layers.clear();
}

void Scene::init()
{
    auto *defaultLayer = new LayerNode();
    defaultLayer->transform.name = "default";
    layers.push_back(defaultLayer);
}

std::string Scene::name() const
{
    return _name;
}

void Scene::clear()
{
    for(auto& item : nodes) {
        delete item;
    }
    nodes.clear();

    for(auto& item : layers) {
        delete item;
    }
    layers.clear();

    for(auto& item : meshes) {
        delete item;
    }
    meshes.clear();

    for(auto& item : cameras) {
        delete item;
    }
    cameras.clear();

    for(auto& item : enviroments) {
        delete item;
    }
    enviroments.clear();
    
    _name = "";

    init();
}

NoOwnershipOf(LayerNode) Scene::getDefaultLayer() const
{
    return layers[0];
}

void Scene::apply(std::function<void(CameraNode&)> func)
{
    for(auto item : cameras) {
        func(*item);
    }
}

void Scene::apply(std::function<void(MeshNode&)> func)
{
    for(auto item : meshes) {
        func(*item);
    }
}

void Scene::apply(std::function<void(const MeshNode&)> func) const
{
    for(auto item : meshes) {
        func(*item);
    }
}

void Scene::applyTransformations()
{
    for(auto& item : layers) {
        item->transform.apply();
    }
}

NoOwnershipOf(EnviromentNode) Scene::findEnviroment(std::string name) const
{
    for(auto& item : enviroments) {
        if(item->transform.name == name) {
            return item;
        }
    }
    return nullptr;
}

const Enviroment& Scene::getDefaultEnviroment() const
{
    return defaultEnviroment;
}

} // ns valo