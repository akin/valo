
#ifndef VALO_SOLVER_H
#define VALO_SOLVER_H

#include <common.h>
#include <gfx/color.h>

namespace valo {

class Ray;
class Mesh;
class Texture;

color::HDR_RGBA solveCube(const Texture& texture , const glm::vec4& direction);

} // ns valo

#endif // VALO_SIMPLERENDERER_H
