
#ifndef VALO_NODE_H
#define VALO_NODE_H

#include <common.h>
#include "transform.h"

namespace valo {

class Node {
private:
public:
    Node();
    virtual ~Node();

    Transform transform;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_NODE_H
