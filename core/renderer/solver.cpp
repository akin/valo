
#include "solver.h"
#include <util.h>
#include <ray.h>
#include <gfx/mesh.h>
#include <gfx/texture.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>

namespace valo {
/*
bool solveTriangle(
    const Mesh& mesh , 
    const Ray& ray, 
    size_t& face,
    glm::vec4& hitPosition, 
    glm::vec4& hitNormal);

bool solveHit(
    const Mesh& mesh , 
    const Ray& ray, 
    size_t& face, 
    glm::vec4& hitPosition, 
    glm::vec4& hitNormal)
{
    switch(mesh.primitive)
    {
        case Primitive::triangle :
            return solveTriangle(mesh, ray, face, hitPosition, hitNormal);
        default:
            break;
    }
    return false;
}

bool solveTriangle(
    const Mesh& mesh , 
    const Ray& ray, 
    size_t& face,
    glm::vec4& hitPosition, 
    glm::vec4& hitNormal)
{
    // Bruteforce triangle detection..
    glm::vec3 position(ray.position);
    glm::vec3 direction(ray.direction);
    glm::vec3 currentHitPosition;

    bool currentHit = false;
    double currentDistance = 0;

    bool hit = false;
    double distance = 0.0f;
    for(Index index = 0 ; (index + 2) < mesh.indexes.size() ; index += 3)
    {
        glm::vec3 const &a = mesh.vertexes[mesh.indexes[index + 0]];
        glm::vec3 const &b = mesh.vertexes[mesh.indexes[index + 1]];
        glm::vec3 const &c = mesh.vertexes[mesh.indexes[index + 2]];

        currentHit = glm::intersectLineTriangle(
            position, 
            direction, 
            a,
            b,
            c,
            currentHitPosition);

        currentDistance = glm::distance(position, currentHitPosition);
        
        if(currentHit && ((!hit) || currentDistance < distance)) {
            face = index / 3;
            hitPosition = glm::vec4(currentHitPosition, 1.0f);
            hitNormal = glm::vec4(glm::normalize(glm::cross(c - a, b - a)), 0.0f);

            distance = currentDistance;
            hit = true;
        }
    }
    return hit;
}
*/

// https://en.wikipedia.org/wiki/Cube_mapping
void convert_cube_uv_to_xyz(Index index, float u, float v, float& x, float& y, float& z)
{
    // convert range 0 to 1 to -1 to 1
    float uc = 2.0f * u - 1.0f;
    float vc = 2.0f * v - 1.0f;
    switch (index)
    {
        case 0: x =  1.0f; y =    vc; z =   -uc; break;	// POSITIVE X
        case 1: x = -1.0f; y =    vc; z =    uc; break;	// NEGATIVE X
        case 2: x =    uc; y =  1.0f; z =   -vc; break;	// POSITIVE Y
        case 3: x =    uc; y = -1.0f; z =    vc; break;	// NEGATIVE Y
        case 4: x =    uc; y =    vc; z =  1.0f; break;	// POSITIVE Z
        case 5: x =   -uc; y =    vc; z = -1.0f; break;	// NEGATIVE Z
    }
}

void convert_xyz_to_cube_uv(float x, float y, float z, Index& index, float& u, float& v)
{
    float absX = glm::abs(x);
    float absY = glm::abs(y);
    float absZ = glm::abs(z);

    float maxAxis, uc, vc;
    if(absX >= absY && absX >= absZ) {
        if(x > 0) {
            // POSITIVE X
            // u (0 to 1) goes from +z to -z
            // v (0 to 1) goes from -y to +y
            maxAxis = absX;
            uc = -z;
            vc = y;
            index = 0;
        }
        else {
            // NEGATIVE X
            // u (0 to 1) goes from -z to +z
            // v (0 to 1) goes from -y to +y
            maxAxis = absX;
            uc = z;
            vc = y;
            index = 1;
        }
    }
    if(absY >= absX && absY >= absZ) {
        if(y > 0) {
            // POSITIVE Y
            // u (0 to 1) goes from -x to +x
            // v (0 to 1) goes from +z to -z
            maxAxis = absY;
            uc = x;
            vc = -z;
            index = 2;
        }
        else {
            // NEGATIVE Y
            // u (0 to 1) goes from -x to +x
            // v (0 to 1) goes from -z to +z
            maxAxis = absY;
            uc = x;
            vc = z;
            index = 3;
        }
    }
    if(absZ >= absX && absZ >= absY) {
        if (z > 0) {
            // POSITIVE Z
            // u (0 to 1) goes from -x to +x
            // v (0 to 1) goes from -y to +y
            maxAxis = absZ;
            uc = x;
            vc = y;
            index = 4;
        }
        else {
            // NEGATIVE Z
            // u (0 to 1) goes from +x to -x
            // v (0 to 1) goes from -y to +y
            maxAxis = absZ;
            uc = -x;
            vc = y;
            index = 5;
        }
    }

    // Convert range from -1 to 1 to 0 to 1
    u = 0.5f * (uc / maxAxis + 1.0f);
    v = 0.5f * (vc / maxAxis + 1.0f);
}

void indexUVtoUV(Index index, float& u, float &v)
{
    //  0,+Y, 0, 0
    // -X,+Z,+X,-Z
    //  0,-Y, 0, 0,

    float uMul = 1.0f / 4.0f;
    float vMul = 1.0f / 3.0f;

    switch(index) {
        case 0: { 
            // +X
            u = (2.0f + u) * uMul;
            v = (1.0f + v) * vMul;
            break;
        }
        case 1: { 
            // -X
            u = (0.0f + u) * uMul;
            v = (1.0f + v) * vMul;
            break;
        }
        case 2: { 
            // +Y
            u = (1.0f + u) * uMul;
            v = (0.0f + v) * vMul;
            break;
        }
        case 3: { 
            // -Y
            u = (1.0f + u) * uMul;
            v = (2.0f + v) * vMul;
            break;
        }
        case 4: { 
            // +Z
            u = (1.0f + u) * uMul;
            v = (1.0f + v) * vMul;
            break;
        }
        case 5: { 
            // -Z
            u = (3.0f + u) * uMul;
            v = (1.0f + v) * vMul;
            break;
        }
    }
}

color::HDR_RGBA solveCube(const Texture& texture , const glm::vec4& direction)
{
    float u,v;
    size_t index;
    convert_xyz_to_cube_uv(direction.x, direction.y, direction.z, index, u, v);

    indexUVtoUV(index, u, v);

    return texture.getAtUv(u,v);
}

} // ns valo
