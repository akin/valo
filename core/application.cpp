#include "application.h"
#include <json.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include "logging.h"
#include "util.h"
#include "scene/loader.h"
#include "scene/scenedebugger.h"
#include "jsontool.h"

#include "renderer/renderer.h"
#include "renderer/simplerenderer.h"

namespace valo {
using json = nlohmann::json;

Application::Application()
: configFile()
, logFile()
, renderer(nullptr)
, animation(scene)
{
}

Application::~Application()
{
    delete renderer;
    renderer = nullptr;
}

void Application::setLogFile(std::string filename)
{
    logFile = filename;
}

void Application::setConfigFile(std::string filename)
{
    configFile = filename;
}

bool Application::configure()
{
    logging::Severity level = logging::verbose;
    int count = 1;
    size_t maxSize = 1000000;
    logging::init(level, logFile.c_str(), maxSize, count); 
    LOG(logging::verbose) << "Starting program";

    // Parse config
    LOG(logging::verbose) << "Parsing " << configFile;

    json root;
    try {
        std::ifstream stream(configFile);
        if(stream.fail()){
            //File does not exist code here
            throw std::runtime_error("Config file does not exist.");
        }
        root << stream;
    }
    catch(const std::runtime_error& error) {
        LOG(logging::error) << error.what();
        return false;
    }
    catch(...) {
        LOG(logging::error) << "Parsing failed.";
        return false;
    }

    std::string configPath = getPathFromFile(configFile);
    settings.configPath = configPath;
    std::string configName;
    if(!read(root, "name", configName)) {
        LOG(logging::error) << "No name specified in config file (" << configFile << ")";
        return false;
    }

    settings.outputPath = "output/";
    auto settingsIter = root.find("settings");
    if(settingsIter != root.end()) {
        auto& settingsObj = settingsIter.value();
        if(settingsObj.is_object()) {
            read(settingsObj,"output", settings.outputPath);

            Millisecond duration;
            if(read(settingsObj,"durationms", duration)) {
                animation.setDuration(duration);
            }
        }
    }
    
    LOG(logging::verbose) << "Loading config '" << configName << "'";

    auto filesIter = root.find("files");
    if(filesIter == root.end()) {
        LOG(logging::error) << "No files specified in config file (" << configFile << ")";
        return false;
    }

    // Now instruct to load files.
    Loader loader(scene, animation);
    loader.setRoot(configPath);
    for (auto& element : *filesIter) {
        std::string file = element;
        std::string sceneFile = configPath + file;

        LOG(logging::verbose) << "Loading scene '" << sceneFile << "'";
        if(!loader.load(sceneFile)) {
            LOG(logging::error) << "Loading scene failed (" << sceneFile << ")";
            return false;
        }
        LOG(logging::verbose) << "Loading scene '" << sceneFile << "' completed.";
    }

    auto nodesIter = root.find("nodes");
    if(nodesIter != root.end()) {
        // Found nodes!
        for (auto& node : *nodesIter) {
            if(!loader.load(node)) {
                LOG(logging::error) << "Loading additional scene node failed.";
            }
        }
    }

    if(renderer == nullptr) {
        renderer = new SimpleRenderer(scene, animation, settings);
    }

    return true;
}

int Application::run()
{
    LOG(logging::verbose) << "Running application.";
    scene.applyTransformations();
    
    SceneDebugger debugger;
    debugger.log(scene);

    renderer->render();

    return EXIT_SUCCESS;
}

} // ns valo
