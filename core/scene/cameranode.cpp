
#include "cameranode.h"
#include <logging.h>

namespace valo {

CameraNode::CameraNode()
{
}

CameraNode::~CameraNode()
{
}

void CameraNode::get(OutputSetting& output) const
{
    output = this->output;
}

void CameraNode::get(ClearSetting& clear) const
{
    clear = this->clear;
}

float CameraNode::getNear() const
{
    return min;
}

float CameraNode::getFar() const
{
    return max;
}
    
void CameraNode::setNear(float val)
{
    min = val;
}

void CameraNode::setFar(float val)
{
    max = val;
}

glm::ivec2 CameraNode::getResolution() const
{
    return resolution;
}

glm::dvec2 CameraNode::getViewSize() const
{
    return viewSize;
}

const glm::mat4& CameraNode::getProjection() const
{
    return projection;
}

void CameraNode::log() const
{
    LOG(logging::verbose) << "Camera '" << transform.name << "'. "
        << "View size: " << viewSize.x << "x" << viewSize.y << ". "
        << "Resolution: " << resolution.x << "x" << resolution.y << ". ";
}

} // ns valo