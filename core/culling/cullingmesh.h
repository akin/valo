
#ifndef VALO_CULLINGMESH_H
#define VALO_CULLINGMESH_H

#include <common.h>

namespace valo {

class Ray;
class Result;
class Mesh;
class CullingNode;
class CullingMesh {
private:
    CullingNode *node;
    const Mesh* mesh;
public:
    CullingMesh();
    ~CullingMesh();

    void clear();

    void build(const Mesh& mesh);
    bool solve(const Ray& ray, Result& result) const;
};

} // ns valo

#endif // VALO_CULLINGMESH_H
