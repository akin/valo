
#ifndef VALO_CAMERANODE_H
#define VALO_CAMERANODE_H

#include <common.h>
#include <misc.h>
#include "node.h"

namespace valo {

class CameraNode : public Node{
private:
    // unfortunately micro$oft has defined near and far, so we can not use them, as if we do, this will not compile.
    float min; // near
    float max; // far

    glm::dvec2 viewSize; 
    glm::ivec2 resolution;
    glm::mat4 projection;
    OutputSetting output;
    ClearSetting clear;
public:
    CameraNode();
    ~CameraNode();

    void get(OutputSetting& output) const;
    void get(ClearSetting& clear) const;
    
    void setNear(float val);
    void setFar(float val);

    float getNear() const;
    float getFar() const;
    glm::ivec2 getResolution() const;
    glm::dvec2 getViewSize() const;
    const glm::mat4& getProjection() const;

    void log() const;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_NODE_H
