
#include "threadtools.h"

#if defined(__unix__)
# define USE_PTHREAD
#endif

#if defined(USE_PTHREAD)
# include <pthread.h>
#endif

namespace valo {

// http://en.cppreference.com/w/cpp/thread/thread/native_handle
bool setPriority(std::thread& thread, size_t priority)
{
#if defined(USE_PTHREAD)
    sched_param sch;
    int policy; 
    pthread_getschedparam(thread.native_handle(), &policy, &sch);
    sch.sched_priority = priority;

    return pthread_setschedparam(thread.native_handle(), SCHED_FIFO, &sch) == 0;
#else
    return false;
#endif
}

size_t getPriority(std::thread& thread)
{
#if defined(USE_PTHREAD)
    sched_param sch;
    int policy; 
    pthread_getschedparam(thread.native_handle(), &policy, &sch);
    return sch.sched_priority;
#else
    return 0;
#endif
}

} // ns valo
