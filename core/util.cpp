
#include "util.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <logging.h>

// includes slash
std::string getPathFromFile(const std::string& file)
{
    size_t pos = file.find_last_of("/\\");
    if(pos == std::string::npos) {
        return file;
    }
    return file.substr(0,pos + 1);
}

std::string getFilenameFromFile(const std::string& file)
{
    size_t pos = file.find_last_of("/\\");
    return file.substr(pos+1);
}

float dpi2dpmm( float dpi )
{
	return dpi * DPI2DPMM;
}

float dpmm2dpi( float dpmm )
{
	return dpmm * DPMM2DPI;
}

void replaceAll(std::string& input, std::string match, std::string replacement)
{
    std::string::size_type n = 0;
    while ( ( n = input.find( match, n ) ) != std::string::npos )
    {
        input.replace( n, match.size(), replacement );
        n += replacement.size();
    }
}

#if defined(OS_MAC)
// https://raw.githubusercontent.com/NickStrupat/CacheLineSize/master/CacheLineSize.c
// http://stackoverflow.com/questions/39680206/understanding-stdhardware-destructive-interference-size-and-stdhardware-cons
// http://stackoverflow.com/questions/794632/programmatically-get-the-cache-line-size

#include <sys/sysctl.h>
size_t getCacheLineSize() {
    size_t lineSize = 0;
    size_t sizeOfLineSize = sizeof(lineSize);
    sysctlbyname("hw.cachelinesize", &lineSize, &sizeOfLineSize, 0, 0);
    return lineSize;
}

#elif defined(OS_WINDOWS)

#include <stdlib.h>
#include <windows.h>
size_t getCacheLineSize() {
	size_t lineSize = 0;
	DWORD bufferSize = 0;
	DWORD i = 0;
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION * buffer = 0;

	GetLogicalProcessorInformation(0, &bufferSize);
	buffer = (SYSTEM_LOGICAL_PROCESSOR_INFORMATION *) malloc(bufferSize);
	GetLogicalProcessorInformation(&buffer[0], &bufferSize);

	for (i = 0; i != bufferSize / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); ++i) {
		if (buffer[i].Relationship == RelationCache && buffer[i].Cache.Level == 1) {
			lineSize = buffer[i].Cache.LineSize;
			break;
		}
	}

	free(buffer);
	return lineSize;
}

#elif defined(OS_LINUX)

#include <stdio.h>
size_t getCacheLineSize() {
	FILE * p = 0;
	p = fopen("/sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size", "r");
	unsigned int lineSize = 0;
	if (p) {
		fscanf(p, "%d", &lineSize);
		fclose(p);
	}
	return lineSize;
}
#else
size_t getCacheLineSize() {
    return 64;
}
#endif

namespace valo {
    ID runtimeGlobalId = 1000;

    ID generateId()
    {
        return ++runtimeGlobalId;
    }

    bool makePath(std::string path)
    {
        std::stringstream ss;
        ss  << "mkdir " << "\"" << path << "\"";

        return runCommand(ss.str());
    }

    std::string templateStr(std::string tpl, const std::map<std::string, std::string>& args)
    {
        for(auto& iter : args) {
            replaceAll(tpl, std::string("{" + iter.first + "}") , iter.second);
        }
        return tpl;
    }

    bool runCommand(std::string command)
    {
        LOG(logging::verbose) << "Running command '" << command << "'";

        system(command.c_str());

        return true;
    }

    bool runCommand(std::string command, const std::map<std::string, std::string>& args)
    {
        return runCommand(templateStr(command, args));
    }

    std::string toString(Millisecond time)
    {
        Millisecond ms = time % 1000;
        int seconds = time / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;

        std::string delim(":");
        std::stringstream ss;
        ss  << std::setfill('0')
            << std::setw(4) << days << delim 
            << std::setw(2) << hours << delim
            << std::setw(2) << minutes << delim
            << std::setw(2) << seconds << delim
            << std::setw(3) << ms;

        return ss.str();
    }

    std::map<Filetype, std::string> formatMapper = {
        {Filetype::none, "unknown"},
        {Filetype::png, "png"},
        {Filetype::bmp, "bmp"},
        {Filetype::tif, "tif"},
        {Filetype::tga, "tga"},
        {Filetype::hdr, "hdr"},
        {Filetype::obj, "obj"},
        {Filetype::json, "json"},
        {Filetype::txt, "txt"}
    };

    std::string toString(Filetype format)
    {
        auto iter = formatMapper.find(format);
        if(iter == formatMapper.end()) {
            return "unknown";
        }
        return iter->second;
    }

    std::string generateFilename(std::string filename, Millisecond time, Filetype format)
    {
        std::stringstream ss;
        ss << filename << "_" << toString(time) << "." << toString(format);
        return ss.str();
    }

    bool resolveRaySphere( const Ray& ray , const glm::vec4& position , float radius , glm::vec4& hitpos )
    {
        const glm::vec4 relation = ray.position - position;
        float s = glm::dot(relation , ray.direction);
        float l2 = glm::dot(relation , relation);
        
        float s2 = s * s;
        float r2 = radius * radius;
        float m2 = l2 - s2;
        
        if( (s < 0.0f && l2 > r2) || (m2 > r2) )
        {
            return false;
        }
        
        float q = glm::sqrt( r2 - m2 );
        float t = (l2 > r2) ? (s - q) : (s + q);
        
        hitpos = ray.position + (ray.direction * t);
        hitpos.w = 1.0f;
        
        return true;
    }
}