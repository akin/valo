
#ifndef VALO_SETTINGS_H
#define VALO_SETTINGS_H

#include <common.h>

namespace valo {

class Settings {
private:
    std::string rootPath;
    std::string configPath;
    std::string outputPath;
public:
    std::string getOutputFor(std::string path) const;
    std::string getOutputPath() const;

    LoaderClass Application;
};

} // ns valo

#endif // VALO_SETTINGS_H
