
#ifndef VALO_LOADER_H
#define VALO_LOADER_H

#include <common.h>

// Libraries exposed at .h
// Tried to avoid it, but fighting it on headers
// that have limited inclusion scope, is insignificant,
// compared to benefits.
#include <json.hpp>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing fla

namespace valo {
using json = nlohmann::json;

class Transform;
class Scene;
class Animation;
class Texture;
class Loader {
protected:
    std::string root;
    Scene& scene;
    Animation& animation;
    std::map<size_t, size_t> localTextureIdMap;
    std::map<size_t, size_t> localMaterialIdMap;
    std::map<size_t, size_t> localMeshIdMap;

    // general
    OwnershipOf(Texture) loadTexture(std::string path);

    // assimp
    bool importTextures(const aiScene *aiscene);
    bool importMaterials(const aiScene *aiscene);
    bool importMeshes(const aiScene *aiscene);
    void parseNodes(const aiNode *ainode, Transform& parent);

    // json
    bool loadTransform(const json& node, Transform& transform);
    bool loadProjection(const json& node, glm::mat4& projection, glm::dvec2& viewSize, float& min, float& max);
    bool loadCamera(const json& node);
    bool loadEnviroment(const json& node);
public:
    Loader(Scene& scene, Animation& animation);
    ~Loader();

    void setRoot(std::string root);
    void clear();

    bool load(const std::string path);
    bool load(const json& node);
};

} // ns valo

#endif // VALO_LOADER_H
