
#ifndef VALO_THREADPOOL
#define VALO_THREADPOOL

#include <iostream>
#include <thread>
#include <functional>

#include "queue.h"

namespace valo {

class ThreadPool
{
private:
    Queue<std::function<void ()>> queue;
    std::vector<std::thread> threads;
    std::atomic<bool> alive;
protected:
    void entry(size_t i);
    void join();
public:
    ThreadPool();
    ThreadPool(size_t count);
    ~ThreadPool();

    size_t size() const;
    
    void kill();
    void clear();
    void run(std::function<void ()> work);
};

} // ns valo

#endif // VALO_THREADPOOL