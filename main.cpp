//
//  main.cpp
//  Lumina
//
//  Created by Mikael Korpela on 20/11/2016.
//  Copyright © 2016 Mikael Korpela. All rights reserved.
//

#include "config.h"
#include <iostream>
#include <core/application.h>
#include <tclap/CmdLine.h>

int main(int argc, const char * argv[]) {
    // Parse command line args.
    valo::Application app;
    try {  
        // Define the command line object, and insert a message
        // that describes the program. The "Command description message" 
        // is printed last in the help text. The second argument is the 
        // delimiter (usually space) and the last one is the version number. 
        // The CmdLine object parses the argv array based on the Arg objects
        // that it contains. 
        TCLAP::CmdLine cmd("Command description message", ' ', "0.9");

        // Define a value argument and add it to the command line.
        // A value arg defines a flag and a type of value that it expects,
        // such as "-n Bishop".
        TCLAP::ValueArg<std::string> configArg("c","config","config file to use",false,"","string");
        TCLAP::ValueArg<std::string> logArg("l","log","log file to use",false,"","string");

        // Add the argument nameArg to the CmdLine object. The CmdLine object
        // uses this Arg to parse the command line.
        cmd.add( configArg );
        cmd.add( logArg );

        // Parse the argv array.
        cmd.parse( argc, argv );

        std::string configFile = configArg.getValue();
        std::string logFile = logArg.getValue();

        if(!configFile.empty()) {
            app.setConfigFile(configFile);
        }
        else {
            app.setConfigFile(VALO_DEFAULT_CONFIG);
        }
        if(!logFile.empty()) {
            app.setLogFile(logFile);
        }
        else {
            app.setLogFile(VALO_DEFAULT_LOG);
        }
    }
    catch (TCLAP::ArgException &e)  // catch any exceptions
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    }
    // Init 
    if(!app.configure()) {
        return EXIT_FAILURE;
    }

    return app.run();
}
