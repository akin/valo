
#ifndef VALO_CACHE_H
#define VALO_CACHE_H

#include <common.h>
#include <gfx/mesh.h>
#include <culling/cullingmesh.h>

namespace valo {

class Cache {
private:
    std::map<ID, CullingMesh> meshCache;
public:
    Cache();
    ~Cache();

    void clear();

    void cache(const Mesh& mesh);
    const CullingMesh &get(const Mesh& mesh) const;
};

} // ns valo

#endif // VALO_CACHE_H
