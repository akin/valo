
#ifndef VALO_SCENEDEBUGGER_H
#define VALO_SCENEDEBUGGER_H

#include <common.h>

namespace valo {

class Transform;
class Scene;
class SceneDebugger {
public:
    SceneDebugger();
    ~SceneDebugger();

    void log(Transform &transform, size_t depth = 0);
    void log(Scene &scene);
};

} // ns valo

#endif // VALO_SCENEDEBUGGER_H
