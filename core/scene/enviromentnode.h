
#ifndef VALO_ENVIROMENTNODE_H
#define VALO_ENVIROMENTNODE_H

#include <common.h>
#include "node.h"
#include "enviroment.h"

namespace valo {

class Texture;
class EnviromentNode : public Node {
protected:
    Enviroment enviroment;
public:
    EnviromentNode();
    ~EnviromentNode();

    const Enviroment& getEnviroment() const;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_ENVIROMENTNODE_H
