#ifndef VALO_BUILD_CONFIG
#define VALO_BUILD_CONFIG
// This file gets included by CMAKE and parsed by CMAKE.
// Place all your odd configs here!
// NOTE!!! This file gets copied when running CMAKE
//  so just running 'make' doesnt copy this file to build process!!

#define VALO_DEFAULT_LOG        "log.csv"
#define VALO_DEFAULT_CONFIG     "resources/config.json"

#endif // VALO_BUILD_CONFIG