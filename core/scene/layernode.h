
#ifndef VALO_LAYERNODE_H
#define VALO_LAYERNODE_H

#include <common.h>
#include "node.h"

namespace valo {

class LayerNode : public Node {
public:
    LayerNode();
    ~LayerNode();

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_LAYERNODE_H
