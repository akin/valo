
#ifndef VALO_GFXUTIL_H
#define VALO_GFXUTIL_H

#include <types.h>
#include "imagebuffer.h"
#include "color.h"

namespace valo {
    void convert(ImageBuffer<color::HDR_RGBA>& source, ImageBuffer<color::LDR_RGBA>& target, bool overflow = false);
    void convert(ImageBuffer<color::LDR_RGBA>& source, ImageBuffer<color::HDR_RGBA>& target);

    color::HDR_RGBA normalToColor(const glm::vec3& normal);

    bool save(
        std::string path, 
        Filetype type,
        const ImageBuffer<color::LDR_RGBA>& source
        );
    
    bool load(
        std::string path,
        Filetype& type,
        ImageBuffer<color::LDR_RGBA>& target
        );
}

#endif // VALO_COLOR_H
