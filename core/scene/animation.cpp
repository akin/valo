#include "animation.h"
#include "scene.h"
#include "cameranode.h"
#include "meshnode.h"

#include <glm/gtc/matrix_transform.hpp> // matrix lookAt & projections

namespace valo {

Animation::Animation(Scene& scene)
: scene(scene)
, position(0)
, end(0)
{
}

Animation::~Animation()
{
}

void Animation::reset()
{
    seek(0);
}

void Animation::setDuration(Millisecond duration)
{
    end = duration;
}

Millisecond Animation::length() const
{
    return end;
}

Millisecond Animation::at() const
{
    return position;
}

bool Animation::finished() const
{
    return position > end;
}

void Animation::apply()
{
    Second seconds = position * MILLIS2S;
    scene.apply([&](MeshNode& node){
        
        node.transform.position = glm::vec3(
            0.0f,
            -0.12f, 
            0.1f);
            

        node.transform.rotation = glm::angleAxis(
            glm::radians(seconds * 180.f), 
            glm::vec3(
                0.0f,
                1.0f, 
                0.f));
    });
    
    scene.apply([&](CameraNode& node){
        Second point = glm::distance(1.0f, seconds);
            
        node.transform.position = glm::vec3(
            0.0f, 
            0.0f,
            -0.5f);
            /*
        node.transform.rotation = glm::angleAxis(
            glm::radians(seconds * 45.f), 
            glm::vec3(
                0.0f,
                1.0f, 
                0.f));
                */
    });
    /*
    scene.apply([&](CameraNode& camera){
        if(position < 1000) {
            camera.transform.rotation = glm::angleAxis(
                glm::radians(position * 0.001f * 360.f), 
                glm::vec3(
                    0.0f,
                    1.0f, 
                    0.f));
        }
        else {
            Millisecond val = position - 1000;
            camera.transform.rotation = glm::angleAxis(
                glm::radians(val * 0.001f * 360.f), 
                glm::vec3(
                    1.0f,
                    0.0f, 
                    0.f));
        }
    });
    */

    scene.applyTransformations();
}

void Animation::seek(Millisecond position)
{
    this->position = position;
    apply();
}

void Animation::advance(Millisecond amount)
{
    position += amount;
    apply();
}

} // ns valo

