
#ifndef VALO_THREADTOOLS
#define VALO_THREADTOOLS

#include <thread>

namespace valo {

bool setPriority(std::thread& thread, size_t priority);
size_t getPriority(std::thread& thread);

} // ns valo

#endif // VALO_THREADTOOLS