
#include "boundingbox.h"
#include <ray.h>

namespace valo {

bool BoundingBox::intersects(const Ray& ray) const
{
   float t[9];
   t[1] = (min.x - ray.position.x)/ray.direction.x;
   t[2] = (max.x - ray.position.x)/ray.direction.x;
   t[3] = (min.y - ray.position.y)/ray.direction.y;
   t[4] = (max.y - ray.position.y)/ray.direction.y;
   t[5] = (min.z - ray.position.z)/ray.direction.z;
   t[6] = (max.z - ray.position.z)/ray.direction.z;

   t[7] = glm::max(glm::max(glm::min(t[1], t[2]), glm::min(t[3], t[4])), glm::min(t[5], t[6]));
   t[8] = glm::min(glm::min(glm::max(t[1], t[2]), glm::max(t[3], t[4])), glm::max(t[5], t[6]));
   
   return (t[8] < 0 || t[7] > t[8]) ? false : true;
}

} // ns valo
