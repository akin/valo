
#ifndef VALO_MESHNODE_H
#define VALO_MESHNODE_H

#include <common.h>
#include <functional>
#include "node.h"

namespace valo {

class Mesh;
class MeshNode : public Node {
private:
    std::vector<NoOwnershipOf(Mesh)> meshes;
public:
    MeshNode();
    ~MeshNode();

    void apply(std::function<void(Mesh&)> func);
    void apply(std::function<void(const Mesh&)> func) const;

    LoaderClass Loader;
    DebugClass SceneDebugger;
};

} // ns valo

#endif // VALO_MESHNODE_H
