
#include "threadpool.h"
#include <logging.h>

namespace valo {

// Optimal amount of threads is hw_concurrency -1
// leave 1 thread to doodle around for the rest..
ThreadPool::ThreadPool()
: ThreadPool(std::max(1u, std::thread::hardware_concurrency() -1))
{
}

ThreadPool::ThreadPool(size_t count)
: alive(true)
{
    if(count == 0) {
        count = 1;
    }
    threads.reserve(count);
    for(size_t i = 0; i < count; ++i) {
        threads.emplace_back(std::bind(&ThreadPool::entry, this, i));
    }
}

ThreadPool::~ThreadPool ()
{
    // Wait for all threads to stop
    kill();
}

size_t ThreadPool::size() const
{
    return threads.size();
}

void ThreadPool::join() 
{
    // Wait for all threads
    for(auto& thread : threads) {
        thread.join();
    }
}

void ThreadPool::entry(size_t i) 
{
    std::function<void()> work;
    while(alive.load()) {
        if(queue.waitPop(work)) {
            work();
        }
    }
}

void ThreadPool::kill()
{
    alive = false;
    queue.clear();
    queue.invalidate();
    join();
}

void ThreadPool::clear()
{
    queue.clear();
}

void ThreadPool::run(std::function<void()> work)
{
    queue.push(std::move(work));
}

} // ns valo
